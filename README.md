# elastic-logging

## Getting started

Before you start using Elastic Stack, you have to configure some options.

- Elasticsearch password in `.env`, defaults to `changeme` (username is `elastic`, do not change).

## Running with low resources
By default the stack runs only logspout with logstash and writes the logs to a .json file. 

**Remember to chown the output folder if Docker-compose creates the volume folder (at `~/logstash_output`)!**

### To start the logging service:

We are using docker-compose as the running environment. When running this logging stack and Signing API in the same environment, starting order doesn't really matter since Logspout is able to automatically attach to all running containers.
```
docker-compose up
```

Starting the stack will take some time and Logspout will try to restart until Elasticsearch is up and can be reached.

### Accessing the logs

When the whole stack is up and running, the collected information can be accessed (with your configured credentials) from the Kibana endpoint at port `5601`. 

When starting for the first time, you have to create an index pattern in Kibana by navigating to the `Discover` tab. An example index pattern is `logstash-*` which includes all of the logs. Select `@timestamp` as the time filter. Now navigate to the `Discover` tab again to view the logged data.

The actual data is stored in the `elastic-data` folder at the root of this project.

### Used ports

The stack uses the following ports:

```
Kibana          5601
Elasticsearch   9200
Elasticsearch   9300
Logstash        5000
Logstash        9600
```
